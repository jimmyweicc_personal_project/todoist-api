import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TaskStatus } from './task-status.enum';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { TasksRepository } from './tasks.repository';
import { Task } from './task.entity';
import { User } from 'src/auth/user.entity';
import { StatusesRepository } from 'src/statuses/statuses.repository';

@Injectable()
export class TasksService {
  private logger = new Logger('StatusesRepository', true);

  constructor(
    @InjectRepository(TasksRepository)
    private tasksRepository: TasksRepository,
    @InjectRepository(StatusesRepository)
    private statusesRepository: StatusesRepository,
  ) {}

  getTasks(filterDto: GetTasksFilterDto, user: User): Promise<Task[]> {
    return this.tasksRepository.getTasks(filterDto, user);
  }

  async getTaskById(id: string, user: User): Promise<Task> {
    const found = await this.tasksRepository.findOne({ where: { id, user } });
    if (!found) {
      throw new NotFoundException(`Task with ID "${id}" not found`);
    }
    return found;
  }

  async createTask(createTaskDto: CreateTaskDto, user: User): Promise<Task> {
    const result = this.tasksRepository.createTask(createTaskDto, user);
    if (result) {
      const status = await this.statusesRepository.getStatusByOrder(0, user);
      const oldTasks = status.tasks;
      oldTasks.push((await result).id);
      this.statusesRepository.save(status);
    }
    return result;
  }

  async updateTaskTitle(id: string, title: string, user: User): Promise<Task> {
    const task = await this.getTaskById(id, user);
    task.title = title;
    task.updateTime = new Date();
    await this.tasksRepository.save(task);
    return task;
  }

  async updateTaskStatus(
    id: string,
    status: TaskStatus,
    user: User,
  ): Promise<Task> {
    const task = await this.getTaskById(id, user);
    task.status = status;
    task.updateTime = new Date();

    if (status === TaskStatus.THIRD) {
      await this.deleteTask(id, user);
      // change task's content
      task.user = user;
      await this.tasksRepository.save(task);

      // append to final status
      const toStatus = await this.statusesRepository.getStatusByOrder(
        TaskStatus.THIRD,
        user,
      );
      const oldTasks = toStatus.tasks;
      oldTasks.push(task.id);
      this.statusesRepository.save(toStatus);
    }
    await this.tasksRepository.save(task);
    return task;
  }

  async deleteTask(id: string, user: User): Promise<void> {
    // modify task's status
    const task = await this.tasksRepository.findOne({ id });

    // delete task
    const result = await this.tasksRepository.delete({ id, user });

    if (result.affected === 0) {
      throw new NotFoundException(`Task with ID ${id} not found`);
    } else {
      const status = task.status;
      const found = await this.statusesRepository.getStatusByOrder(
        status,
        user,
      );
      const newTasks = found.tasks.filter((task) => task !== id);
      found.tasks = newTasks.length === 0 ? [] : newTasks;
      await this.statusesRepository.save(found);
    }
  }
}
