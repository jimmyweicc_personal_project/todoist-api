import { IsString } from 'class-validator';
import { TaskStatus } from '../task-status.enum';

export class UpdateTaskTitleDto {
  @IsString()
  title: string;
}
