import {
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { User } from 'src/auth/user.entity';
import { Status } from './status.entity';
import { GetStatusesFilterDto } from './dto/get-statuses-filter.dto';
import { CreateStatusDto } from './dto/create-status.dto';

@EntityRepository(Status)
export class StatusesRepository extends Repository<Status> {
  private logger = new Logger('StatusesRepository', true);

  async getStatuses(
    filterDto: GetStatusesFilterDto,
    user: User,
  ): Promise<Status[]> {
    const { order } = filterDto;
    const query = this.createQueryBuilder('status');

    query.where({ user });

    if (order) {
      query.andWhere('(LOWER(status.order) LIKE LOWER(:order))', {
        order: `%${order}%`,
      });
    }

    try {
      const statuses = await query.getMany();
      return statuses;
    } catch (error) {
      this.logger.error(
        `Failed to get statuses for user "${
          user.username
        }". Filters: ${JSON.stringify(filterDto)}`,
        error.stack,
      );
      throw new InternalServerErrorException();
    }
  }

  async getStatusByOrder(order: number, user: User): Promise<Status> {
    try {
      const found = await this.findOne({
        where: { order, user },
      });
      if (!found) {
        throw new NotFoundException(`Status with ORDER "${order}" not found`);
      }
      return found;
    } catch (error) {
      this.logger.error(
        `Failed to get statuses for user "${user.username} of order "${order}"`,
        error.stack,
      );
      throw new InternalServerErrorException();
    }
  }

  async createStatus(
    createStatusDto: CreateStatusDto,
    user: User,
  ): Promise<Status> {
    const { order, title } = createStatusDto;

    const status = this.create({
      order,
      title,
      tasks: [],
      user,
    });

    await this.save(status);
    return status;
  }
}
