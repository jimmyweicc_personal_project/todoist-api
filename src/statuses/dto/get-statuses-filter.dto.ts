import { IsNumber, IsOptional } from 'class-validator';

export class GetStatusesFilterDto {
  @IsOptional()
  @IsNumber()
  order?: number;
}
