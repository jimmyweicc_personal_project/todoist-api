import { IsNotEmpty } from 'class-validator';

export class CreateStatusDto {
  @IsNotEmpty()
  order: number;

  @IsNotEmpty()
  title: string;
}
