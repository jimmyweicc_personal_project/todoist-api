import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'src/auth/auth.module';
import { StatusesController } from './statuses.controller';
import { StatusesRepository } from './statuses.repository';
import { StatusesService } from './statuses.service';

@Module({
  imports: [TypeOrmModule.forFeature([StatusesRepository]), AuthModule],
  controllers: [StatusesController],
  providers: [StatusesService],
  exports: [StatusesService]
})
export class StatusesModule {}
