import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/auth/user.entity';
import { CreateStatusDto } from './dto/create-status.dto';
import { GetStatusesFilterDto } from './dto/get-statuses-filter.dto';
import { Status } from './status.entity';
import { StatusesRepository } from './statuses.repository';

@Injectable()
export class StatusesService {
  constructor(
    @InjectRepository(StatusesRepository)
    private statusesRepository: StatusesRepository,
  ) {}

  async getStatuses(
    filterDto: GetStatusesFilterDto,
    user: User,
  ): Promise<Status[]> {
    return this.statusesRepository.getStatuses(filterDto, user);
  }

  async getStatusByOrder(order: number, user: User): Promise<Status> {
    return this.statusesRepository.getStatusByOrder(order, user);
  }

  createStatus(createStatusDto: CreateStatusDto, user: User): Promise<Status> {
    return this.statusesRepository.createStatus(createStatusDto, user);
  }

  async updateStatusTitle(
    order: number,
    title: string,
    user: User,
  ): Promise<Status> {
    const status = await this.getStatusByOrder(order, user);
    status.title = title;
    await this.statusesRepository.save(status);
    return status;
  }

  async updateStatusTasks(
    order: number,
    tasks: string[],
    user: User,
  ): Promise<Status> {
    const status = await this.getStatusByOrder(order, user);
    status.tasks = tasks;
    await this.statusesRepository.save(status);
    return status;
  }
}
