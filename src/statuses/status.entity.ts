import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Exclude } from 'class-transformer';
import { User } from 'src/auth/user.entity';

@Entity()
export class Status {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  order: number;

  @Column()
  title: string;

  @Column('simple-array', { nullable: true })
  tasks: string[];

  @ManyToOne((_type) => User, (user) => user.statuses, { eager: false })
  @Exclude({ toPlainOnly: true })
  user: User;
}
