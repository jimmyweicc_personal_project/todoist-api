import {
  Body,
  Controller,
  Get,
  Logger,
  Param,
  Patch,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/auth/get-user.decorator';
import { User } from 'src/auth/user.entity';
import { GetStatusesFilterDto } from './dto/get-statuses-filter.dto';
import { UpdateStatusTitleDto } from './dto/update-status-title.dto';
import { UpdateStatusTasksDto } from './dto/update-statuses-tasks.dto';
import { Status } from './status.entity';
import { StatusesService } from './statuses.service';

@Controller('statuses')
@UseGuards(AuthGuard())
export class StatusesController {
  private logger = new Logger('StatusesController');

  constructor(private statusesService: StatusesService) {}

  @Get()
  getStatuses(
    @Query() filterDto: GetStatusesFilterDto,
    @GetUser() user: User,
  ): Promise<Status[]> {
    this.logger.verbose(
      `User "${
        user.username
      }" retrieving all statuses. Filters: ${JSON.stringify(filterDto)}`,
    );
    return this.statusesService.getStatuses(filterDto, user);
  }

  @Get('/:order')
  getStatusByOrder(
    @Param('order') order: number,
    @GetUser() user: User,
  ): Promise<Status> {
    return this.statusesService.getStatusByOrder(order, user);
  }

  @Patch('/:order/title')
  updateStatusTitle(
    @Param('order') order: number,
    @Body() updateStatusTitleDto: UpdateStatusTitleDto,
    @GetUser() user: User,
  ): Promise<Status> {
    const { title } = updateStatusTitleDto;
    return this.statusesService.updateStatusTitle(order, title, user);
  }

  @Patch('/:order/tasks')
  updateStatusTasks(
    @Param('order') order: number,
    @Body() updateStatusTasksDto: UpdateStatusTasksDto,
    @GetUser() user: User,
  ): Promise<Status> {
    const { tasks } = updateStatusTasksDto;
    return this.statusesService.updateStatusTasks(order, tasks, user);
  }
}
