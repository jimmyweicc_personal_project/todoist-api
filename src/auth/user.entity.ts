import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Status } from 'src/statuses/status.entity';
import { Task } from 'src/tasks/task.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true })
  username: string;

  @Column()
  password: string;

  @OneToMany((_type) => Status, (status) => status.user, { eager: true })
  statuses: Status[];

  @OneToMany((_type) => Task, (task) => task.user, { eager: true })
  tasks: Task[];
}
