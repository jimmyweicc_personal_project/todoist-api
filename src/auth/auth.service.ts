import {
  Injectable,
  InternalServerErrorException,
  Logger,
  UnauthorizedException,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { UsersRepository } from './users.repository';
import { JwtPayload } from './jwt-payload.interface';
import { StatusesRepository } from 'src/statuses/statuses.repository';
import { TaskStatus } from '../tasks/task-status.enum';

@Injectable()
export class AuthService {
  private logger = new Logger('AuthService', true);

  constructor(
    @InjectRepository(UsersRepository)
    private userRepository: UsersRepository,
    @InjectRepository(StatusesRepository)
    private statusRepository: StatusesRepository,
    private jwtService: JwtService,
  ) {}

  async signUp(authCredentialsDto: AuthCredentialsDto): Promise<void> {
    const user = this.userRepository.createUser(authCredentialsDto);

    try {
      this.statusRepository.createStatus(
        { order: 0, title: 'Backlog' },
        await user,
      );
      this.statusRepository.createStatus(
        { order: 1, title: 'In Progress' },
        await user,
      );
      this.statusRepository.createStatus(
        { order: 2, title: 'Done' },
        await user,
      );
    } catch (error) {
      this.logger.error('[auth/AuthService/signUp()] error', error.stack);
      throw new InternalServerErrorException();
    }
  }

  async signIn(
    authCredentialsDto: AuthCredentialsDto,
  ): Promise<{ accessToken: string }> {
    const { username, password } = authCredentialsDto;
    const user = await this.userRepository.findOne({ username });

    if (user && (await bcrypt.compare(password, user.password))) {
      const payload: JwtPayload = { username };
      const accessToken: string = await this.jwtService.sign(payload);
      return { accessToken };
    } else {
      throw new UnauthorizedException('Please check your login credentials');
    }
  }
}
